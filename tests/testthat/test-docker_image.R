
dat <- data.frame(list(mpg = 20, wt = 10))

res_correct <- tryCatch({
httr::POST('http://localhost:8000/score', 
           body = list(input_data = dat),
           encode = 'json') }, 
warning = function(w) {
    message(w)
    return(NULL)
  },
   error = function(e) {
     message(e) 
     return(-1)
})

res_incorrect <- tryCatch({
httr::POST('localhost:8001/score', 
           body = list(input_data = dat),
           encode = 'json') }, 
warning = function(w) {
    message(w)
    return(NULL)
  },
   error = function(e) {
     message(e) 
     return(-1)
})



test_that("Check whether connection works", {
  expect_equal(class(res_correct), 'response')
  expect_equal(class(res_incorrect), 'numeric')
})

